<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pozo extends Model
{
    protected $table = 'pozos';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}