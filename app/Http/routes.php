<?php

$app->get('/', function () use ($app) {
    return $app->welcome();
});

$app->get('posts', 'PostController@index');

$app->get('posts/{id}', 'PostController@show');

$app->post('posts', 'PostController@store');

$app->put('posts/{id}', 'PostController@update');

$app->delete('posts/{id}', 'PostController@destroy');


$app->get('pozos', 'PozoController@index');

$app->get('{code}/pozos', 'PozoController@index_by_student');

$app->get('pozos/favorites', 'PozoController@favorites');

$app->get('pozos/{id}', 'PozoController@show');

$app->post('{code}/pozos', 'PozoController@store');

$app->put('pozos/{id}', 'PozoController@update');

$app->delete('pozos/{id}', 'PozoController@destroy');


$app->post('animes/like/{id}', 'AnimeController@vote');

$app->get('{code}/animes', 'AnimeController@index');

$app->get('{code}/animes/favorites', 'AnimeController@favorites');

$app->get('{code}/animes/{id}', 'AnimeController@show');

$app->put('{code}/animes/{id}', 'AnimeController@add_favorite');

$app->get('comments/{id}', 'CommentController@index');

$app->post('{code}/comments/{id}', 'CommentController@store');

$app->delete('comments/{id}', 'CommentController@delete');






