<?php namespace App\Http\Controllers;

use App\Pozo;
use App\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Tests\Controller;
use App\Http\Controllers\Controller as CoreController;

class PozoController extends CoreController
{
    public function index()
    {
        $result = Pozo::with(['user'])->get()->map(function ($item) {
            return $this->mapPozo($item);
        });

        return response()->json($result);
    }

    public function index_by_student($code)
    {
        $user = User::where('code', $code)->first();

        $result = Pozo::with(['user'])->where('user_id', $user->id)->get()->map(function ($item) {
            return $this->mapPozo($item);
        });

        return response()->json($result);
    }

    public function favorites()
    {
        $result = Pozo::with(['user'])->where('is_favorite', 1)->get()->map(function ($item) {
            return $this->mapPozo($item);
        });

        return response()->json($result);
    }

    public function store(Request $request, $code)
    {
        $user = User::where('code', $code)->first();

        $pozo = new Pozo();

        $pozo->user_id = $user->id;
        $pozo->observations = $request->input('observations');
        $pozo->latitude = $request->input('latitude');
        $pozo->longitude = $request->input('longitude');
        $pozo->name = $request->input('name');
        $pozo->operator = $request->input('operator');
        $pozo->is_favorite = false;

        $pozo->save();

        return response()->json($pozo);
    }

    public function show($id)
    {
        return $this->mapPozo(Pozo::find($id));
    }

    public function  update($id, Request $request)
    {
        $pozo = Pozo::find($id);
        $pozo->is_favorite = $request->input('is_favorite');

        $pozo->save();

        return $pozo;
    }

    public function destroy($id)
    {
        Pozo::find($id)->delete();

        return response()->json(true);
    }

    private function mapPozo($item)
    {
        return [
            'id'           => $item->id,
            'name'         => $item->name,
            'operator'     => $item->operator,
            'observations' => $item->observations,
            'created_at'   => $item->created_at->format('m/d/Y H:m'),
            'is_favorite'  => $item->is_favorite,
            'created_by'   => $item->user ? $item->user->names : null,
        ];
    }
}