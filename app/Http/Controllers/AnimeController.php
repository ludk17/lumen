<?php namespace App\Http\Controllers;

use App\Anime;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Symfony\Component\HttpKernel\Tests\Controller;
use App\Http\Controllers\Controller as CoreController;

class AnimeController extends CoreController
{
    public function index($code)
    {
        $user = User::with('animes')->where('code', $code)->first();

        $animes = Anime::all()->map(function($item) use($user){

            if($user->animes->contains('id', $item->id))
                $item->is_favorite = true;
            else
                $item->is_favorite = false;

            return $item;
        });

        return response()->json($animes);
    }

    public function show($code, $id)
    {
        $user = User::with('animes')->where('code', $code)->first();

        $anime = Anime::find($id);

        if($user->animes->contains('id', $id))
            $anime->is_favorite = true;
        else
            $anime->is_favorite = false;

        return $anime;
    }

    public function add_favorite($code, $id, Request $request)
    {
        $is_favorite = $request->input('is_favorite');

        $user =  User::with('animes')->where('code', $code)->first();

        if($is_favorite == 'true')
            $user->animes()->attach($id);
        else
            $user->animes()->detach($id);

    }

    public function favorites($code)
    {
        $user = User::with('animes')->where('code', $code)->first();
        return response()->json($user->animes->map(function($item){
            $item->is_favorite = true;
            return $item;
        }));
    }

    public function vote($id)
    {
        $anime = Anime::find($id);
        $anime->votes += 1;

        return $anime;

    }
}
