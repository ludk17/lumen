<?php namespace App\Http\Controllers;


use App\Comment;
use App\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Tests\Controller;
use App\Http\Controllers\Controller as CoreController;

class CommentController extends CoreController
{
    public function index($id)
    {
        $comments = Comment::with('user')->where('anime_id', $id)->get()->map(function ($item) {
            $item->created_by = $item->user->names;
            unset($item->user);

            return $item;
        });

        return response()->json($comments);
    }

    public function store($code, $id, Request $request)
    {
        $user = User::where('code', $code)->first();

        $comment = Comment::create([
            'user_id'  => $user->id,
            'anime_id' => $id,
            'comment'  => $request->input('comment')
        ]);

        return $comment;

    }


    public function delete($id)
    {

        Comment::find($id)->delete();

    }
}