<?php namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Tests\Controller;
use App\Http\Controllers\Controller as CoreController;

class PostController extends CoreController
{
    public function index()
    {
        return response()->json(Post::all());
    }

    public function store(Request $request)
    {
        $post = new Post();

        $post->user = $request->input('user');
        $post->name = $request->input('name');
        $post->content = $request->input('content');

        $post->save();

        return response()->json($post);
    }

    public function show($id)
    {
        return Post::find($id);
    }

    public function  update($id, Request $request)
    {
        $post = Post::find($id);
        $post->name = $request->input('name');
        $post->user = $request->input('user');
        $post->content = $request->input('content');


        $post->save();
        return $post;
    }

    public function destroy($id)
    {
        Post::find($id)->delete();
        return response()->json(true);
    }
}
