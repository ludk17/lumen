<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';

    protected $guarded = [];

    public function animes()
    {
        return $this->belongsToMany(Anime::class, 'user_anime');
    }
}