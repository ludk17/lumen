<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(['code' => '701212', 'names' => 'MENDOZA ESCALANTE, LUIS']);
        User::create(['code' => '705255', 'names' => 'BRIONES CHILON, NOE ELIAS']);
        User::create(['code' => '707000', 'names' => 'CARMONA AQUINO, KINVERLI']);
        User::create(['code' => '706314', 'names' => 'FLORES MARTOS, CINDY YASMIN']);
        User::create(['code' => '705715', 'names' => 'GALLARDO YNTOR, HENRY ALEJANDRO']);
        User::create(['code' => '703708', 'names' => 'GONZALES SALDAÑA, MILAGROS JANETH']);
        User::create(['code' => '705631', 'names' => 'HERNANDEZ VALENCIA, ERICK OMAR']);
        User::create(['code' => '705500', 'names' => 'IPARRAGUIRRE RUIZ, MAX DARWIN']);
        User::create(['code' => '705046', 'names' => 'MARIN CORTEZ, JULIO CESAR']);
        User::create(['code' => '708041', 'names' => 'MEJIA VASQUEZ, EDWAR']);
        User::create(['code' => '704002', 'names' => 'OCAS DIAZ, ESTELA ESMERALDA']);
        User::create(['code' => '713423', 'names' => 'PERALES ORE, LILIBETH GABRIELA']);
        User::create(['code' => '706173', 'names' => 'PEREDA CABANILLAS, EDWUARD JHONATAN']);
        User::create(['code' => '704763', 'names' => 'RABANAL SANGAY, JHAN PIERD']);
        User::create(['code' => '705025', 'names' => 'SAAVEDRA GUARNIZ, OSCAR GENY']);
        User::create(['code' => '703802', 'names' => 'SALDAÑA ALCANTARA, SARA BETTY']);
        User::create(['code' => '704703', 'names' => 'TAICA SANCHEZ, RUTH YOVANY']);
        User::create(['code' => '700876', 'names' => 'TUSE GUTIERREZ, MAYRA FIOREL']);
        User::create(['code' => '704792', 'names' => 'VIGO TERRONES, JESUS LISSET']);
        User::create(['code' => '703824', 'names' => 'YEPEZ CABANILLAS, DIDIER GERSON']);

    }
}
