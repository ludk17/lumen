<?php

use App\Anime;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AnimeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('animes')->truncate();

        Anime::create(['title' => 'Fairy Tail', 'votes' => 0, 'episodes' => 441, 'type' => 'Serie', 'sinopsis' => 'Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años', 'image_url' => 'ft' ]);
        Anime::create(['title' => 'One Piece', 'votes' => 0,'episodes' => 990, 'type' => 'Serie', 'sinopsis' => 'Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años', 'image_url' => 'op' ]);
        Anime::create(['title' => 'Dragon Ball Super', 'votes' => 0,'episodes' => 22, 'type' => 'Serie', 'sinopsis' => 'Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años', 'image_url' => 'dbs' ]);
        Anime::create(['title' => 'One Punch Man', 'votes' => 0,'episodes' => 11, 'type' => 'Serie', 'sinopsis' => 'Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años', 'image_url' => 'opm' ]);
        Anime::create(['title' => 'Katekyo Hitam Reborn', 'votes' => 0, 'episodes' => 222, 'type' => 'Serie', 'sinopsis' => 'Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años', 'image_url' => 'khr' ]);
        Anime::create(['title' => 'Death Note', 'votes' => 0, 'episodes' => 10, 'type' => 'Serie', 'sinopsis' => 'Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años', 'image_url' => 'dn' ]);
        Anime::create(['title' => 'Code Guess R2', 'votes' => 0, 'episodes' => 22, 'type' => 'Serie', 'sinopsis' => 'Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años', 'image_url' => 'cg' ]);
        Anime::create(['title' => 'Shingeki no Kyojin', 'votes' => 0, 'episodes' => 441, 'type' => 'Serie', 'sinopsis' => 'Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años', 'image_url' => 'sk' ]);
        Anime::create(['title' => 'Sword Art Online', 'votes' => 0, 'episodes' => 20, 'type' => 'Serie', 'sinopsis' => 'Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años', 'image_url' => 'sao' ]);
        Anime::create(['title' => 'Bleach', 'votes' => 0, 'episodes' => 200, 'type' => 'Serie', 'sinopsis' => 'Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años', 'image_url' => 'bleach' ]);
    }
}
